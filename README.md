# Lisez-moi #

### Pourquoi ce document ###

* L'objectif de ce document et de fournir un support pour une formation de 2 heures à l'écriture d'articles scientifiques pour des étudiants et nouveaux professionels de la recherche en biologie. 
* Ce site à pour objectif de faciliter les échanges avec les étudiants souhaitant donner un retour, et apporter des suggestions ou des corrections. 

### Comment récupérer ce document ? ###
[Téléchargez le pdf](https://bitbucket.org/cmbce/formationredactionarticlescientifique/raw/master/presentation_cmbc.pdf)

### Comment puis-je participer ? ###

* Proposez les suggestions et corrections à travers l'[outil de signalement de bugs](https://bitbucket.org/cmbce/formationredactionarticlescientifique/issues?status=new&status=open)
* Si vous maîtrisez GIT, vous pouvez faire un clone et faire des "pull requests"
* Ce document est sous forme de fichier LaTeX (beamer). Vous devriez pouvoir le reproduire simplement en installant LaTeX sur votre ordinateur. Si vous voulez proposer des modifications importantes il serait bien de compiler avant de faire une "pull request".
